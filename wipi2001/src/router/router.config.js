import { lazy, Component } from 'react';
export const indexRoutes = [
  {
    path: '/article',
    meta: {
      //路由元信息
      title: 'article',
    },
    element: lazy(() => import('@/pages/index/article/article')),
  },
  {
    path: '/file',
    meta: {
      title: 'file',
    },
    element: lazy(() => import('@/pages/index/file/file')),
  },
  {
    path: '/konwledge',
    meta: {
      title: 'konwledge',
    },
    element: lazy(() => import('@/pages/index/konwledge/konwledge')),
  },
];
const routes = {
  //   mode: 'hash',
  routes: [
    {
      path: '/',
      element: lazy(() => import('@/pages/index/index')),
      children: [
        ...indexRoutes,
        {
          path: '/',
          to: '/article',
        },
        {
          path: '*',
          element: lazy(() => import('@/pages/index/nofind/nofind')),
        },
      ],
    },
  ],
};

export default routes;
