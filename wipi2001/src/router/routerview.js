import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  HashRouter,
} from 'react-router-dom';
import React, { Suspense } from 'react';
import routerConfig from './router.config';
const BeforeRoutes = ({ item }) => {
  document.title = item.meta?.title || '八维创作平台';
  return item.to ? <Navigate to={item.to} /> : <item.element />;
};
const renderRoutes = (routes) => {
  return routes.map((item) => (
    <Route
      path={item.path}
      key={item.path}
      element={<BeforeRoutes item={item} />}
    >
      {item.children && renderRoutes(item.children)}
    </Route>
  ));
};

const RouterView = () => {
  const { mode = 'history', routes } = routerConfig;
  return mode === 'history' ? (
    <BrowserRouter>
      <Suspense loading={<>loading...</>}>
        <Routes>{renderRoutes(routes)}</Routes>
      </Suspense>
    </BrowserRouter>
  ) : (
    <HashRouter>
      <Suspense loading={<>loading...</>}>
        <Routes>{renderRoutes(routes)}</Routes>
      </Suspense>
    </HashRouter>
  );
};

export default RouterView;
