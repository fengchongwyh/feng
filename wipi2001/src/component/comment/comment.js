import React from 'react';
import CommentModule from './comment.module.less';
import * as dayjs from 'dayjs';
import * as RelativeTime from 'dayjs/plugin/relativeTime'; // 导入插件
import 'dayjs/locale/zh-cn'; // 导入本地化语言
import { randomColor } from '@/utils/color';
import Reply from '@/component/reply/reply';

dayjs.extend(RelativeTime); // 使用插件
dayjs.locale('zh-cn'); // 使用本地化语言
const Comment = ({ list, handleChangeReply }) => {
  // console.log(list, 'list');
  const handleReply = (item) => {
    handleChangeReply(item);
  };
  return (
    <>
      {list &&
        list.map((item) => (
          <div className={CommentModule.comment} key={item.id}>
            <div className={CommentModule.header}>
              <span style={{ background: randomColor() }}>
                {item.name.slice(0, 1)}
              </span>
              <div>
                <span>{item.name}</span>
                {item.replyUserName && <div>回复{item.replyUserName}</div>}
              </div>
            </div>
            <div className={CommentModule.content}>{item.content}</div>
            <div className={CommentModule.footer}>
              <span>{item.userAgent}</span>
              <span>{dayjs(new Date(item.updateAt)).fromNow()}</span>
              <span onClick={() => handleReply(item)}>回复</span>
            </div>
            {item.isflag && <Reply />}
            {item.children && (
              <div className={CommentModule.nestcomment}>
                <Comment list={item.children} />
              </div>
            )}
          </div>
        ))}
    </>
  );
};

export default Comment;
