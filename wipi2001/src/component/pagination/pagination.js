import React, { useState, memo } from 'react';
import PaginationModule from './pagination.module.less';
const Pagination = memo(({ total, page = 1, pageSize = 5, onChange }) => {
  console.log('Pagination');
  const count = Math.ceil(total / pageSize); //一共多少页
  const [ind, setInd] = useState(0);
  // console.log(count, new Array(2), 'new Array(count).fill(count)');
  const changePage = (ind) => {
    //点击页码数
    onChange(ind);
  };
  const handlerLeft = () => {
    // < 1 2 3(当前) >
    page > 1 && onChange(page - 1);
  };
  const handerRight = () => {
    // < 1(当前) 2 3 >
    page < count && onChange(page + 1);
  };
  return (
    <div className={PaginationModule.pagination}>
      <div>
        <span onClick={handlerLeft} className={PaginationModule.left}>
          &lt;
        </span>
        {count &&
          new Array(count).fill(count).map((item, index) => (
            <span
              key={index}
              onClick={() => changePage(index + 1)}
              className={page == index + 1 ? PaginationModule.active : ''}
            >
              {index + 1}
            </span>
          ))}
        <span onClick={handerRight} className={PaginationModule.left}>
          &gt;
        </span>
      </div>
    </div>
  );
});

export default Pagination;
