import React from 'react';
import { observer } from 'mobx-react-lite';

const Count = ({ store }) => {
  const { count, addCount, delCount } = store;
  const changeCountAdd = () => {
    addCount();
  };
  const changeCountDel = () => {
    delCount();
  };
  return (
    <div>
      <h3>{count}</h3>
      <button onClick={changeCountAdd}>加加</button>
      <button onClick={changeCountDel}>减减</button>
    </div>
  );
};

export default observer(Count);
