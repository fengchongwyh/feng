import axios from 'axios';
import ErrorBound from './logs';
// 1. 请求超时  2.区分环境 （开发环境、测试环境、预发布环境、生产环境） 3.收集错误信息  4.超时的处理
const httpTool = ({
  timeout = 10,
  commonHeaders = {},
  failMesage = (msg) => {
    alert(msg);
  },
}) => {
  const httpAxios = axios.create({
    // timeout: timeout * 10000,
    baseURL: process.env.REACT_APP_BASE_URL,
    retry: 3, //设置最大次数
    retryDelay: 1000, //设置重新请求等待的时间
  });

  // 添加请求拦截器
  httpAxios.interceptors.request.use(
    function (config) {
      // 在发送请求之前做些什么
      return {
        ...config,
        headers: {
          ...config.headers,
          ...commonHeaders,
        },
      };
    },
    function (err) {
      return Promise.reject(err);
    }
  );

  // 添加响应拦截器
  httpAxios.interceptors.response.use(
    function (response) {
      // 对响应数据做点什么
      return response;
    },
    function (err) {
      // 对响应错误做点什么
      // 请求超时的处理
      console.log(err, 'error');
      // 对请求错误做些什么
      let config = err.config; // 如果config不存在或未设置重试选项，请拒绝
      console.log(config, 'config');
      if (!config || !config.retry) return Promise.reject(err); // 设置变量跟踪重试次数
      config.__retryCount = config.__retryCount || 0; // 检查是否已经达到最大重试总次数
      if (config.__retryCount >= config.retry) {
        // 抛出错误信息
        //给用户一个友好的信息提示
        failMesage('请求超时');
        //错误收集、埋点
        ErrorBound({
          type: 'axios',
          error: {
            method: err.config.method,
            url: err.config.url,
          },
        });
        return Promise.reject(err);
      } // 增加请求重试次数
      config.__retryCount += 1; // 创建新的异步请求

      let backoff = new Promise(function (resolve) {
        setTimeout(function () {
          resolve();
        }, config.retryDelay || 1);
      });
      // 返回axios信息，重新请求
      return backoff.then(function () {
        return httpAxios(config);
      });
    }
  );

  return httpAxios;
};

export default httpTool;
