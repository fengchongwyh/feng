export const randomParams = () => {
  return Math.floor(Math.random() * 256);
};
export const randomColor = () => {
  const opacity = Math.random().toFixed(2); // 透明度不需要太多位小数，两位就够了
  const color = `rgb(${randomParams()},${randomParams()},${randomParams()},${opacity})`;
  return color;
};
