const article = {
  getRecommend: {
    method: 'get',
    url: '/api/article/recommend/',
  },
  getArticle: {
    method: 'get',
    url: '/api/article/',
  },
  getComment: {
    method: 'get',
    url: '/api/comment/host/',
  },
};

export default article;
