// import article from './modules/article';
// import file from './modules/file';

// console.log({ ...article, ...file });

// api.getArticle()
//require.context(路径，是否需要递归，规则)
import axios from 'axios';
import httpTool from '@/utils/httpTool';
import { message } from 'antd';
const httpAxios = httpTool({
  timeout: 1000,
  commonHeader: {
    token: localStorage.getItem('token') || '123',
  },
  failMesage: (msg) => {
    message.warn(msg);
  },
});
const context = require.context('@/api/modules', false, /\.js$/);

console.dir(context.keys(), 'context');

//批量导入、合并对象
const dataObj = context.keys().reduce((prev, cur) => {
  //prev上一次的值  cur是当前项()
  console.log(context(cur).default);
  const def = { ...prev, ...context(cur).default };
  return def;
}, {});
// dataObj= {
//   getArticle: { method: 'get', url: '/api/article' },
//   getFile: { method: 'get', url: '/api//api/file' },
//   getRecommend: { method: 'get', url: '/api/article/recommend' },
//   getaa: { method: 'get', url: '/api/article/recommend' },
// };
// ['getArticle','getFile','getRecommend']
const api = Object.keys(dataObj).reduce((prev, cur) => {
  prev[cur] = (data, id) =>
    httpAxios({
      ...dataObj[cur],
      url: id ? dataObj[cur].url + id : dataObj[cur],
      [dataObj[cur].method === 'get' ? 'params' : 'data']: data,
    });

  return prev;
}, {});

console.log(api);

// const api = {
//   getRecommend: (data) => axios({ method: 'get', url: '' }),
//   getArticle: () => axios(),
// };

// api.getArticle({page:1});
console.log(dataObj);

export default api;

// const aa = (params)=>axios.get('/aa',parmas)
