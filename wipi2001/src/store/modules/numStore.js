import { makeAutoObservable } from 'mobx';
class NumStore {
  count = 0;
  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }
  addCount() {
    //action
    console.log('********addcount');
    this.count++;
  }
  delCount() {
    this.count--;
  }
}

export default new NumStore();
