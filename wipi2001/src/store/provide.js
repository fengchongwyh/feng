import { createContext, useContext } from 'react';
export const RootContext = createContext(); //创建一个上下文对象  {Provider,Consumer}
console.log(RootContext, 'RootContext');
const Provider = ({ children, ...props }) => {
  return <RootContext.Provider value={props}>{children}</RootContext.Provider>;
};

export const useRootStore = () => useContext(RootContext);
export default Provider;
