import React, { useState, useEffect } from 'react';
import { useTranslation, Trans } from 'react-i18next';
import i18n from 'i18next';
const File = () => {
  // throw new Error('抛出错误');
  const { t } = useTranslation();
  const [val, setVal] = useState('zh');
  const changeLange = () => {
    setVal(val === 'zh' ? 'en' : 'zh');
  };
  useEffect(() => {
    i18n.changeLanguage(val); // val入参值为'en'或'zh'
  }, [val]);
  return (
    <div>
      <h3>{t('header.home')}</h3>
      <button onClick={changeLange}>国际化</button>
    </div>
  );
};

export default File;
