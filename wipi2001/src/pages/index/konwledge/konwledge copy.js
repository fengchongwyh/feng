import React, { useState, useEffect, useRef } from 'react';
import DataJson from './konwledge.json';
import KonwModule from './konwledge.module.less';
import './markdown.less';
import classname from 'classnames';
import QRCode from 'qrcode.react';
const http = () =>
  new Promise((resolve, reject) => {
    resolve(DataJson);
  });
const Konwledge = () => {
  const [data, setData] = useState({});
  const [ind, setInd] = useState(0);
  const konwledge = useRef(null);
  const tags = ['h1', 'h2', 'h3'];
  const scrollH = useRef(null); // 每一个标题距离页面顶部的距离
  const canvasDom = useRef(null); //canvas
  const [url, setUrl] = useState(''); //弹框里面图片的url路径
  const [flag, setFlag] = useState(false); //控制画布弹框的显示隐藏
  const getlist = async () => {
    let res = await http();
    console.log(res, 'res');
    if (res.statusCode == 200) {
      setData(res.data);
    }
  };
  const changeInd = (ind) => {
    setInd(ind);
    document.documentElement.scrollTop = scrollH.current[ind];
  };
  // 点击分享
  const handlerShare = () => {
    setFlag(!flag);
  };
  //滚动事件
  const scollHandler = () => {
    const scrolY = Math.ceil(document.documentElement.scrollTop); //滚动距离
    scrollH.current.map((item, index) => {
      if (scrolY >= item && scrolY < scrollH.current[index + 1]) {
        setInd(index);
      }
    });
  };
  useEffect(() => {
    getlist();
  }, []);
  // 获取img对象
  const getImage = (imgurl) => {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.crossOrigin = 'Anonymous'; //前端需要做的，要配合后端一起。后端需要设置响应头access-control-allow-origin: *
      img.src = imgurl;
      img.onload = function () {
        resolve(img);
      };
      img.onerror = function (error) {
        reject(error);
      };
    });
  };
  const drawCanvas = async (ctx) => {
    //绘制填充背景
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, 400, 668);
    //绘制标题

    ctx.font = '20px Georgia';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 124, 30);

    //绘制标题底部的线

    ctx.beginPath();
    ctx.strokeStyle = 'green'; // 红色路径
    ctx.moveTo(0, 45);
    ctx.lineTo(400, 45);
    ctx.stroke(); // 进行绘制

    // // 绘制图片的时候没有问题，在导出画布的时候，就会失败。因为外部链接这种，会污染画布。
    let img = await getImage(data.cover);
    ctx.drawImage(img, 50, 130, 300, 300);

    // 绘制图片下边的文字
    ctx.font = '12px Georgia';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 50, 450);
    ctx.fillText(data.summary, 50, 470);

    //绘制二维码
    const qrcode = document.getElementById('qrcode');
    const qrcodeimg = await getImage(qrcode.toDataURL());
    ctx.drawImage(qrcodeimg, 50, 490, 120, 120);

    // 绘制二维码后边的文字
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 190, 500);

    // 生产url 把Canvas转成图片
    const imgUrl = canvasDom.current.toDataURL();
    // console.log(imgUrl, 'imgUrl');
    return imgUrl;

    // console.log(url, 'qrcode');
  };
  const getInitCanvas = async () => {
    canvasDom.current.width = 400;
    canvasDom.current.height = 668;
    const ctx = canvasDom.current.getContext('2d'); //上下文对象
    //绘制
    let imgurl = await drawCanvas(ctx);
    setUrl(imgurl);
    console.log(imgurl, '12313121------------');
  };
  useEffect(() => {
    //获取所有h1  h2 h3标签
    getInitCanvas();
    const child = [...konwledge.current.children].filter((item) =>
      tags.includes(item.nodeName.toLowerCase())
    );
    const lastChild = child[child.length - 1];
    scrollH.current = child.map((item) => item.offsetTop);
    console.log(scrollH.current, 'konwledge');
    lastChild &&
      scrollH.current.push(lastChild.offsetTop + lastChild.offsetHeight);
    console.log(scrollH.current, 'konwledge12312');

    window.addEventListener('scroll', scollHandler, false);
    return () => {
      window.removeEventListener('scroll', scollHandler, false);
    };
  }, [data]);
  return (
    <div className={classname('markdown', KonwModule.konw)}>
      <button onClick={handlerShare}>分享</button>
      <canvas id="canvas" ref={canvasDom} className={KonwModule.show}></canvas>
      <QRCode
        className={KonwModule.show}
        id="qrcode"
        value={window.location.href} //value参数为生成二维码的链接 我这里是由后端返回
        size={200} //二维码的宽高尺寸
      />
      <div
        ref={konwledge}
        className={KonwModule.left}
        dangerouslySetInnerHTML={{ __html: data.html }}
      ></div>
      <div className={KonwModule.comment}></div>
      <div className={KonwModule.right}>
        {data.toc &&
          JSON.parse(data.toc).map((item, index) => (
            <p
              className={ind == index ? KonwModule.active : ''}
              key={item.id}
              onClick={() => changeInd(index)}
            >
              {item.text}
            </p>
          ))}
      </div>
      {flag && (
        <div className={KonwModule.dialog}>
          <img src={url} />
          {/* <button onClick={handlerDownloade}>下载</button> */}
          <a href={url} download="canvas.png">
            下载
          </a>
          <button onClick={() => setFlag(false)}>关闭</button>
        </div>
      )}
    </div>
  );
};

export default Konwledge;
