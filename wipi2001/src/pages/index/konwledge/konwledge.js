import React, { useState, useEffect, useRef, useCallback } from 'react';
import DataJson from './konwledge.json';
import KonwModule from './konwledge.module.less';
import './markdown.less';
import classname from 'classnames';
import QRCode from 'qrcode.react';
import useCanvas from '@/hooks/useCanvas';
import Reply from '@/component/reply/reply';
import Comment from '@/component/comment/comment';
import Pagination from '@/component/pagination/pagination';
import api from '@/api';
const http = () =>
  new Promise((resolve, reject) => {
    resolve(DataJson);
  });
const Konwledge = () => {
  const [data, setData] = useState({});
  const [ind, setInd] = useState(0);
  const konwledge = useRef(null);
  const tags = ['h1', 'h2', 'h3'];
  const scrollH = useRef(null); // 每一个标题距离页面顶部的距离
  const canvasDom = useRef(null); //canvas
  // const [url, setUrl] = useState(''); //弹框里面图片的url路径
  const [flag, setFlag] = useState(false); //控制画布弹框的显示隐藏
  const url = useCanvas(data, canvasDom);
  const [commentList, setCommentList] = useState([]);
  const [pageInfo, setPageInfo] = useState({
    page: 1,
    pageSize: 5,
  }); //分页的初始值
  const getlist = async () => {
    let res = await http();
    console.log(res, 'res');
    if (res.statusCode == 200) {
      setData(res.data);
    }
  };
  const changeInd = (ind) => {
    setInd(ind);
    document.documentElement.scrollTop = scrollH.current[ind];
  };
  // 点击分享
  const handlerShare = () => {
    setFlag(!flag);
  };
  //滚动事件
  const scollHandler = () => {
    const scrolY = Math.ceil(document.documentElement.scrollTop); //滚动距离
    scrollH.current.map((item, index) => {
      if (scrolY >= item && scrolY < scrollH.current[index + 1]) {
        setInd(index);
      }
    });
  };

  //获取评论列表数据
  const getCommentnList = async () => {
    let res = await api.getComment(
      pageInfo,
      '5ad3fe5a-a302-4c3b-a334-b0bb58628aa1'
    );
    console.log(res.data, 'resss');
    if (res.data.statusCode == 200) {
      setCommentList(res.data.data);
    }
  };

  useEffect(() => {
    getlist();
    getCommentnList();
  }, []);

  useEffect(() => {
    //获取所有h1  h2 h3标签

    const child = [...konwledge.current.children].filter((item) =>
      tags.includes(item.nodeName.toLowerCase())
    );
    const lastChild = child[child.length - 1];
    scrollH.current = child.map((item) => item.offsetTop);
    console.log(scrollH.current, 'konwledge');
    lastChild &&
      scrollH.current.push(lastChild.offsetTop + lastChild.offsetHeight);
    console.log(scrollH.current, 'konwledge12312');

    window.addEventListener('scroll', scollHandler, false);
    return () => {
      window.removeEventListener('scroll', scollHandler, false);
    };
  }, [data]);

  // 分页
  const changePagintion = useCallback(
    (page) => {
      setPageInfo({
        ...pageInfo,
        page,
      });
    },
    [setPageInfo]
  );

  //当页码数发生变化的时候，要重新请求数据
  useEffect(() => {
    getCommentnList(); //获取评论数据的接口
  }, [pageInfo]);

  const handleChangeReply = (item) => {
    item.isflag = !item.isflag;
    setCommentList([...commentList]);
  };
  return (
    <div className={classname('markdown', KonwModule.konw)}>
      {/* 海报生成 */}
      <button onClick={handlerShare}>分享</button>
      <canvas id="canvas" ref={canvasDom} className={KonwModule.show}></canvas>
      <QRCode
        className={KonwModule.show}
        id="qrcode"
        value={window.location.href} //value参数为生成二维码的链接 我这里是由后端返回
        size={200} //二维码的宽高尺寸
      />
      {/* 楼层 */}
      <div
        ref={konwledge}
        className={KonwModule.left}
        dangerouslySetInnerHTML={{ __html: data.html }}
      ></div>

      <div className={KonwModule.right}>
        {data.toc &&
          JSON.parse(data.toc).map((item, index) => (
            <p
              className={ind == index ? KonwModule.active : ''}
              key={item.id}
              onClick={() => changeInd(index)}
            >
              {item.text}
            </p>
          ))}
      </div>
      {/* 海报下载 */}
      {flag && (
        <div className={KonwModule.dialog}>
          <img src={url} />
          {/* <button onClick={handlerDownloade}>下载</button> */}
          <a href={url} download="canvas.png">
            下载
          </a>
          <button onClick={() => setFlag(false)}>关闭</button>
        </div>
      )}
      {/* 评论 */}
      <div className={KonwModule.comment}>
        <h1>评论</h1>
        {/* 回复 */}
        <Reply />
        {/* 评论列表 */}
        <Comment list={commentList[0]} handleChangeReply={handleChangeReply} />
        {/* 分页1 */}
        <Pagination
          total={commentList[1]}
          page={pageInfo.page}
          onChange={changePagintion}
        />
      </div>
    </div>
  );
};

export default Konwledge;
