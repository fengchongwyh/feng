import React, { useState, useEffect, useRef } from 'react';
import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';
const Nofind = () => {
  const [time, setTime] = useState(3);
  const timer = useRef(null);
  const navigate = useNavigate();
  // time-1 time--  time = time-1
  useEffect(() => {
    timer.value = setInterval(() => {
      console.log('setInterval');
      setTime((time) => time - 1);
    }, 1000);
    return () => {
      clearInterval(timer.value);
    };
  }, []);

  useEffect(() => {
    if (time <= 0) {
      navigate('/');
    }
  }, [time]);

  const goTohome = () => {
    navigate('/');
  };

  return (
    <div>
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Button type="primary" onClick={goTohome}>
            {time}秒自动回到首页
          </Button>
        }
      />
    </div>
  );
};

export default Nofind;
