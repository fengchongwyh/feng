const promise1 = new Promise((resolve, reject) => {
  console.log('promise1', '----1');

  resolve();
});

promise1.then(() => {
  console.log(3, '----2');
});

console.log('1', promise1, '----3');

const fn = () =>
  new Promise((resolve, reject) => {
    console.log(2, '---4');

    resolve('success');
  });

fn().then((res) => {
  console.log(res, '----5');
});

console.log('start', '----6');
