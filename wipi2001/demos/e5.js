async function async1() {
  console.log('async1 start 1'); // 同2
  await async2(); // await 后侧和上边相当于Promise的构造函数   左侧和下边相当于.then()
  console.log('async1 end 2'); //微1
}
async function async2() {
  // 同3
  console.log('async2 3');
}
console.log('script start 4'); // 同1
setTimeout(function () {
  //宏1
  console.log('setTimeout 5');
}, 0);
async1();
new Promise(function (resolve) {
  console.log('promise1 6'); //同4
  resolve();
}).then(function () {
  // 微2
  console.log('promise2 7');
});
console.log('script end 8'); // 同5

// class Promise{
//   static resolve(){ 静态的方法

//   }
// }
// new  Promise()

// Promise.resolve()

// es7: async awiat
// 1. await只能用在async函数里面  awit后边跟的是一个promise对象。如果跟的不是一个promise对象，它会调Promise.reslove()，把它变成一个promise对象

// async函数本身就是个promise对象
