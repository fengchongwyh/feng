const promise1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    //宏1
    resolve('success');
  }, 1000);
});
const promise2 = promise1.then(() => {
  //微1
  throw new Error('error!!!');
});
console.log('promise1', promise1); //pending
console.log('promise2', promise2); //pending
setTimeout(() => {
  // 宏2
  console.log('promise3', promise1); //pending--resolve
  console.log('promise4', promise2); //pending--reject
}, 2000);
