Promise.resolve().then(() => {
  // 微1
  console.log('promise1');

  const timer2 = setTimeout(() => {
    // 宏2
    console.log('timer2');
  }, 0);
});

const timer1 = setTimeout(() => {
  // 宏1
  console.log('timer3');

  Promise.resolve().then(() => {
    // 微2
    console.log('promise4');
  });
}, 0);

console.log('start5'); // 同步
