const plugins = []; //插件
if (process.env.NODE_ENV === 'production') {
  //如果是生产环境的话，移除console
  plugins.push('transform-remove-console');
}
module.exports = {
  presets: ['react-app'],
  plugins,
};
