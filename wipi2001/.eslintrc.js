module.exports = {
  extends: ['react-app', 'react-app/jest', 'plugin:prettier/recommended'],
  rules: {
    'prettier/prettier': 1,
    'no-console': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-debugger': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-alert': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-var': process.env.NODE_ENV === 'development' ? 1 : 2,
    'no-unused-vars': [1, { vars: 'all', args: 'none' }],
  },
};
