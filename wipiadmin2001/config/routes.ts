export default [
  {
    path: '/',
    component: '@/pages/index/index',
    name: '工作台', // 兼容此写法
    icon: 'CopyOutlined',
  },
  {
    path: '/user',
    component: '@/pages/user/index',
    name: '用户管理', // 兼容此写法
    icon: 'CopyOutlined',
    access: 'canLogin',
  },
  {
    path: '/article',
    name: '文章管理', // 兼容此写法
    icon: 'ChromeOutlined',
    routes: [
      {
        path: '/article/allarticle',
        component: '@/pages/article/allarticle',
        name: '所有文章',
      },
      {
        path: '/article/tagarticle',
        component: '@/pages/article/tagarticle',
        name: '文章标签',
      },
      {
        path: '/article/typearticle',
        component: '@/pages/article/typearticle',
        name: '文章分类',
      },
    ],
    // icon: 'testicon',
  },
  {
    path: '/login',
    component: '@/pages/login/', // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  {
    path: '/registry',
    component: '@/pages/registry/',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
];
