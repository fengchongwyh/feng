import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  routes: routes,
  layout: {
    name: '八维创作平台',
    logo: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fstorage-public.zhaopin.cn%2FCompanyLogo%2F20160920%2F57050C54A0A944E6B71869420F2F7352.jpg&refer=http%3A%2F%2Fstorage-public.zhaopin.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1666593099&t=3d1ee5b4cdab804ee001b246dfdb9049',
  },
  proxy: {
    '/api': {
      target: 'https://creationadmin.shbwyz.com/',
      changeOrigin: true,
    },
  },
  dva: {
    immer: true,
    hmr: false,
  },
});
