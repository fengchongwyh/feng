# umi project

## Getting Started

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```



### 接口
1. 在app.ts里面创建request.
2. 新建services文件夹里面创建文件，在里面定义接口
3. 在页面里面调用）——



### hook使用规则
1. 只在最顶层使用 Hook
2. 不要在循环，条件或嵌套函数中调用 Hook
3. 只在 React 函数中调用 Hook



### 权限
1. 菜单权限
2. 接口权限
3. 按钮权限(react高阶组件、vue通过自定义指令来实现)


```
const isButton = (Button)=>{
    return ()=>{
        render(){
            let role = localstore.getitem('role')
            
            return role !=== 'admin' && <Button>删除</Button>
        }
    }
}
<isButton></isButton>
```


### 菜单权限
1. 新建一个access.ts文件
2. 在里面定义一个方法，该方法返回一个对象，对象里面定义所需要的权限
3. 这个方法需要一个参数，这个参数怎么来？
4. 这个参数需要@umijs/plugin-initial-state这个插件获取



### 纯函数
1. 相同的输入会产生相同的输出
2. 函数在执行过程中没有产生任何副作用