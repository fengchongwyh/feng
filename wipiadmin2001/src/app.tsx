import Header from './component/header';
import Footer from './component/footer';
import { RequestConfig } from 'umi';
import { message } from 'antd';
import { createLogger } from 'redux-logger';

export const layout = () => {
  return {
    rightContentRender: () => <Header />,
    footerRender: () => <Footer />,
    onPageChange: () => {
      console.log('onpagechange');
    },
    menuHeaderRender: undefined,
  };
};

export const request: RequestConfig = {
  timeout: 10000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      console.log(url, options, 'options');
      let token = localStorage.getItem('userInfo')
        ? JSON.parse(localStorage.getItem('userInfo')!).token
        : undefined;
      return {
        url: `${url}`,
        options: {
          ...options,
          headers: {
            ...options.headers,
            authorization: `Bearer ${token}`,
          },
        },
      };
    },
  ],
  responseInterceptors: [
    //相当于之前axios里面的相应拦截器
    (response: any) => {
      const codeMaps = {
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
        400: '请求出错，请重新请求',
        401: '没有登录',
        403: '没有权限',
      };
      //请求成功的话正常返回
      if (response.status == 200 || response.status == 304) {
        return response;
      }
      //不成功需要弹框提示
      message.error(codeMaps[response.status]);
      window.location.href = '/login';
      return;
    },
  ],
};

export const getInitialState = async () => {
  console.log('getInitialState');
  let userInfo = localStorage.getItem('userInfo')
    ? JSON.parse(localStorage.getItem('userInfo')!)
    : { role: 'admin' };
  return userInfo;
};

export const dva = {
  config: {
    onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
