import { useState } from 'react';
interface configType {
  current: number;
  pageSize: number;
  onChange: (current: number, pageSize: number) => void;
}

const defaultConfig = {
  current: 1,
  pageSize: 5,
  onChange: function (current: number) {
    console.log(current, 'defaultConfig');
  },
};
const usePage = (config: configType = defaultConfig) => {
  const [pageInfo, setPageInfo] = useState({
    page: 1,
    pageSize: 5,
  });
  return {
    ...config,
    current: pageInfo.page,
    pageSize: pageInfo.pageSize,
    onChange: function (current: number, pageSize: number) {
      console.log('onchangeusepage', current);
      //点击页码数的时候，要做2件事情。1、更新页码数  2. 重新请求数据（不同页面请求的数据不一样）
      if (config.onChange) {
        //如果传了这个函数，就走传递进来的这个
        config.onChange(current, pageSize);
      }
      //当前hook更新页码
      setPageInfo({
        page: current,
        pageSize,
      });
    },
  };
};

export default usePage;
