import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import usePage from './usePage';
const useList = (fn: any) => {
  const [data, setData] = useState({
    list: [],
    total: 0,
  });
  const pagination = usePage({
    current: 1,
    pageSize: 5,
    onChange: function (current: number, pageSize: number) {
      //请求数据
      console.log(current, 'uselisy');
      getinit({ page: current, pageSize });
    },
  });
  const getinit = async (info: any) => {
    let res = await fn(info);
    if (res.statusCode == 200) {
      setData({
        list: res.data[0],
        total: res.data[1],
      });
    }
    console.log(res, '*****');
  };
  useEffect(() => {
    getinit({ page: 1, pageSize: 5 });
  }, []);

  const { loading, error } = useRequest(getinit);
  return {
    list: data.list,
    total: data.total,
    loading,
    error,
    pagination,
  };
};

export default useList;
