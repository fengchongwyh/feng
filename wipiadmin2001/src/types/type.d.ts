export interface pageParmas {
  page: number;
  pageSize: number;
}
