import React from 'react';
import { searchType } from '../types/type';
import { Space, Table, Tag, Badge } from 'antd';
import dayjs from 'dayjs';

export const searchConfig: searchType = {
  columns: [
    {
      title: '搜索词',
      dataIndex: 'keyword',
      key: 'keyword',
    },
    {
      title: '搜索量',
      dataIndex: 'count',
      key: 'count',
    },

    {
      title: '搜索时间',
      dataIndex: 'updateAt',
      key: 'updateAt',
      render: (text) => (
        <span>{dayjs(new Date(text)).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a>删除</a>
        </Space>
      ),
    },
  ],
};
