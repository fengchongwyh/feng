import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getsearch } from '@/services/search';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { searchConfig } from './config/config';
import useList from '@/hooks/useList';
import TablePanel from '@/component/tabelpanel';
function IndexPage() {
  const { list, total, pagination } = useList(getsearch);
  return (
    <div>
      <TablePanel
        pagination={pagination}
        list={list}
        total={total}
        dataConfig={searchConfig}
      />
    </div>
  );
}

export default IndexPage;
