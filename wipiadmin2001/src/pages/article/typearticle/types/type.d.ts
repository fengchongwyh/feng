import type { ColumnsType } from 'antd/es/table';
export interface DataType {
  keyword: string;
  count: string;
  updateAt: string;
}
export interface searchType {
  columns: ColumnsType<DataType>;
}
