import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getsearch } from '@/services/search';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { pageConfig } from './config/config';
function IndexPage() {
  const [pageInfo, setPageInfo] = useState({
    page: 1,
    pageSize: 5,
  });
  const [data, setData] = useState({
    list: [],
    total: 0,
  });
  const getinit = async () => {
    let res = await getsearch(pageInfo);
    if (res.statusCode == 200) {
      setData({
        list: res.data[0],
        total: res.data[1],
      });
    }
    console.log(res, '*****');
  };
  const { loading, error } = useRequest(getinit);
  const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
    'checkbox',
  );
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
  };
  return (
    <div>
      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        rowKey="id"
        columns={pageConfig.columns}
        dataSource={data && data.list}
      />
    </div>
  );
}

export default IndexPage;
