import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getsearch } from '@/services/search';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { pageConfig } from './config/config';
import useList from '@/hooks/useList';
function IndexPage() {
  const { list, total } = useList(getsearch);
  const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
    'checkbox',
  );
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
  };
  return (
    <div>
      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        rowKey="id"
        columns={pageConfig.columns}
        dataSource={list && list}
      />
    </div>
  );
}

export default IndexPage;
