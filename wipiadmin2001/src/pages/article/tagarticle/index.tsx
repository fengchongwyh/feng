import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getpage } from '@/services/page';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { pageConfig } from './config/config';
import useList from '@/hooks/useList';
import TablePanel from '@/component/tabelpanel';
function IndexPage() {
  const { list, total, page, pageSize } = useList(getpage);

  return (
    <div>
      <TablePanel
        isAllSelected={false}
        scorll={false}
        list={list}
        total={total}
        pageSize={pageSize}
        dataConfig={pageConfig}
      />
    </div>
  );
}

export default IndexPage;
