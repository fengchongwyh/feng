import type { ColumnsType } from 'antd/es/table';
export interface DataType {
  key: string;
  name: string;
  path: string;
  status: string;
  order: number;
  views: number;
  publishAt: string;
}
export interface pageType {
  columns: ColumnsType<DataType>;
}
