import React from 'react';
import { pageType } from '../types/type';
import { Space, Table, Tag, Badge } from 'antd';
import dayjs from 'dayjs';

export const pageConfig: pageType = {
  columns: [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '路径',
      dataIndex: 'path',
      key: 'path',
    },
    {
      title: '顺序',
      dataIndex: 'order',
      key: 'order',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: (text) => <span>{text === 'publish' ? '已发布' : '草稿'}</span>,
    },

    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
      render: (text) => (
        <Badge
          className="site-badge-count-109"
          count={text}
          overflowCount={1000}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'publishAt',
      render: (text) => (
        <span>{dayjs(new Date(text)).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space size="middle">
          <a>编辑</a>
          <a>删除</a>
          <a>下线</a>
        </Space>
      ),
    },
  ],
};
