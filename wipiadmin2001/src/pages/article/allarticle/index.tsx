import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getarticle } from '@/services/article';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { articleConfig } from './config/config';
import useList from '@/hooks/useList';
import TablePanel from '@/component/tabelpanel';
function IndexPage() {
  const { list, total, pagination } = useList(getarticle);
  console.log(list, 'list');
  return (
    <div>
      <TablePanel
        isAllSelected={true}
        scorll={true}
        list={list}
        total={total}
        pagination={pagination}
        dataConfig={articleConfig}
      />
    </div>
  );
}

export default IndexPage;
