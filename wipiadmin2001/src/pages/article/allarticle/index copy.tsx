import { useRequest } from 'umi';
import { useEffect, useState } from 'react';
import { getarticle } from '@/services/article';
import { Table } from 'antd';
import { DataType } from './types/type';
import React from 'react';
import { articleConfig } from './config/config';
function IndexPage() {
  const [pageInfo, setPageInfo] = useState({
    page: 1,
    pageSize: 5,
  });
  const [data, setData] = useState({
    list: [],
    total: 0,
  });
  const getinit = async () => {
    let res = await getarticle(pageInfo);
    if (res.statusCode == 200) {
      setData({
        list: res.data[0],
        total: res.data[1],
      });
    }
    console.log(res, '*****');
  };
  const { loading, error } = useRequest(getinit);
  //全选
  const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
    'checkbox',
  );
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
  };
  return (
    <div>
      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        scroll={{ x: 1500, y: 300 }}
        rowKey="id"
        columns={articleConfig.columns}
        dataSource={data && data.list}
      />
    </div>
  );
}

export default IndexPage;
