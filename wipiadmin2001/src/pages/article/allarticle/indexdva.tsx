import { useDispatch, useSelector } from 'umi';
import { useEffect } from 'react';
function IndexPage() {
  const dispatch = useDispatch();
  const { title, list, total } = useSelector((state: any) => state.article);
  console.log(title, 'state', list, total);
  useEffect(() => {
    dispatch({
      type: 'article/getArticle',
      payload: {
        page: 1,
        pageSize: 5,
      },
    });
  }, []);
  const changeTitle = () => {
    //更新仓库的值
    dispatch({
      type: 'article/changeTitle',
      payload: {
        title: '新的title',
      },
    });
  };
  return (
    <div>
      <h1>{title}</h1>
      <button onClick={changeTitle}>改变title</button>
    </div>
  );
}

export default IndexPage;

// const connect = (state)=>{ 柯里化
//   return (Com)=>{
//     return <Com {...state}>
//   }
// }
