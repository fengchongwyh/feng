import type { ColumnsType } from 'antd/es/table';
export interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
}
export interface articleType {
  columns: ColumnsType<DataType>;
}
