import React from 'react';
import { articleType } from '../types/type';
import { Space, Table, Tag, Badge } from 'antd';
import dayjs from 'dayjs';

export const articleConfig: articleType = {
  columns: [
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
      render: (text) => <a>{text}</a>,
      fixed: 'left',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: (text) => <span>{text === 'publish' ? '已发布' : '草稿'}</span>,
    },
    {
      title: '分类',
      dataIndex: 'category',
      key: 'category',
      render: (text) => (
        <>{text && <Tag color="geekblue">{text.label.toLowerCase()}</Tag>}</>
      ),
    },

    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      render: (_, { tags }) => (
        <>
          {tags.map((tag: any) => {
            let color = tag.label.length > 5 ? 'geekblue' : 'green';
            if (tag.label === 'css') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.label.toLowerCase()}
              </Tag>
            );
          })}
        </>
      ),
    },

    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
      render: (text) => (
        <Badge
          className="site-badge-count-109"
          count={text}
          overflowCount={1000}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      key: 'likes',
      render: (text) => <Badge count={text} />,
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'publishAt',
      render: (text) => (
        <span>{dayjs(new Date(text)).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space size="middle">
          <a>编辑</a>
          <a>删除</a>
        </Space>
      ),
    },
  ],
};
