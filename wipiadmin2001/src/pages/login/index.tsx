import { Button, Checkbox, Form, Input } from 'antd';
import React from 'react';
import { useRequest, useHistory, useModel } from 'umi';
import { login } from '@/services/user';
const Login = () => {
  const { run, loading } = useRequest(login, { manual: true });
  const history = useHistory();
  const { setInitialState } = useModel('@@initialState');
  const onFinish = async (values: any) => {
    //点击登录
    console.log('Success:', values);
    //发接口请求
    // let res = await login(values);
    // console.log(res, 'rs');

    let res = await run(values);
    localStorage.setItem('userInfo', JSON.stringify(res));
    setInitialState(res); //更新状态值
    history.push('/');
    console.log(res, '^^^^^');
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="用户名"
        name="name"
        rules={[
          {
            required: true,
            message: '请输入姓名',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="密码"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          登录
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Login;
