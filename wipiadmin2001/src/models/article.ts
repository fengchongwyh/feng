import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getarticle } from '@/services/article';
export interface ArticleModelState {
  title: string;
  list: any[];
  total: number;
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    getArticle: Effect;
  };
  reducers: {
    changeTitle: Reducer<ArticleModelState>;
    changeData: Reducer<ArticleModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
}
const Article: ArticleModelType = {
  namespace: 'article',
  state: {
    title: '2001A',
    list: [],
    total: 0,
  },
  effects: {
    //effects里面的方法相当于vuex里面的action
    *getArticle({ payload }, { call, put }): any {
      console.log(payload, 'payload');
      let res = yield call(getarticle, payload);
      //更新仓库数据
      yield put({
        type: 'changeData',
        payload: res.data,
      });
    },
  },
  reducers: {
    //reducers里面的方法相当于vuex里面的mutation
    changeTitle(state, action) {
      console.log(state, action, 'changetitle');
      return {
        ...state,
        ...action.payload,
      };
    },
    changeData(state, action) {
      console.log(state, action, 'changeData********************');
      return {
        ...state,
        ...action.payload,
        list: action.payload[0],
        total: action.payload[1],
      };
    },
  },
};

export default Article;
