import { request } from 'umi';
import { pageParmas } from '@/types/type';
//获取搜索列表的接口
export const getsearch = (params: pageParmas) =>
  request('/api/search', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
