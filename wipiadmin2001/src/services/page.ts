import { request } from 'umi';
import { pageParmas } from '@/types/type';
//获取所以页面列表的接口
export const getpage = (params: pageParmas) =>
  request('/api/page', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
