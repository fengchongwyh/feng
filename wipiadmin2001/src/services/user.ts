import { request } from 'umi';

interface LoginParmas {
  name: string;
  password: string;
}

//定义接口
export const login = (data: LoginParmas) =>
  request('/api/auth/login', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
