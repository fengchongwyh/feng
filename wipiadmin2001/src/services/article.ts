import { request } from 'umi';

import { pageParmas } from '@/types/type';

//获取所以文章列表的接口
export const getarticle = (params: pageParmas) =>
  request('/api/article', {
    method: 'get',
    params,
    skipErrorHandler: true,
  });
