import { Table } from 'antd';
import { useState } from 'react';
/**
 *  list:
 *
 */
interface IProps {
  list: any[];
  total: number;
  pageSize?: number;
  dataConfig: any;
  isAllSelected?: boolean;
  scorll?: boolean | undefined;
  pagination: any;
}
const TablePanel = (props: IProps) => {
  const {
    list,
    total,
    pagination,
    dataConfig,
    isAllSelected = true,
    scorll = false,
  } = props;
  //全选
  const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>(
    'checkbox',
  );
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
  };
  return (
    <Table
      rowSelection={
        isAllSelected
          ? {
              type: selectionType,
              ...rowSelection,
            }
          : undefined
      }
      scroll={scorll ? { x: 1500, y: 300 } : undefined}
      pagination={pagination && { ...pagination, total }}
      rowKey="id"
      columns={dataConfig.columns}
      dataSource={list && list}
    />
  );
};

export default TablePanel;
